NYC Schools App

This is a simple app that displays list of schools in NY and their detail with SAT score,
developed using modern techniques like ConstrainLayouts, JetPack Library for ViewModel, 
LiveData, Data Binding, Navigation and third party library’s Retrofit 2.0 for Network calls.

The app contains two screens
1. Schools List View
2. School Detail View

Schools list view fetches the data from the API https://data.cityofnewyork.us/resource/s3k6-pzi2.json
and shows them on the list selecting an item opens the detail page. Detail page shows more imformation 
from the selected school like name, overview, website address alongside fetches the SAT score in background 
and displays it when ready

I have used MVVM Architecture for this project.

Other options had in mind but could not implement due to time limitaion given for this project

1. Paginated fetching the School list. Curently I fetch all the school information 
which is not necessary we can do a paginated as the user scrolls to see more schools

2. More polished UI, Current I displyed only simple details, which is less user engaing. 
Had a thought of doing more action like opening website, displaying location as snap shot

