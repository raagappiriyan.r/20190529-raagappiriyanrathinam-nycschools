package com.raagappiriyan.nycscholls.schools;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.raagappiriyan.nycscholls.data.School;

public class DiffCallback extends DiffUtil.ItemCallback<School> {
    @Override
    public boolean areItemsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return oldItem == newItem;
    }

    @Override
    public boolean areContentsTheSame(@NonNull School oldItem, @NonNull School newItem) {
        return oldItem.mDbn.equals(newItem.mDbn);
    }
}
