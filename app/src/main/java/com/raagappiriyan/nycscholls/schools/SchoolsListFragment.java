package com.raagappiriyan.nycscholls.schools;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.raagappiriyan.nycscholls.data.School;
import com.raagappiriyan.nycscholls.databinding.FragmentSchoolsListBinding;

import java.util.List;

public class SchoolsListFragment extends Fragment {
    private SchoolsListViewModel mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SchoolsListViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final FragmentSchoolsListBinding binding = FragmentSchoolsListBinding.inflate(inflater,
                container, false);
        binding.setViewModel(mViewModel);
        binding.setLifecycleOwner(this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        final SchoolsListAdapter adapter = new SchoolsListAdapter(new SchoolsListAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelected(School school) {
                Navigation.findNavController(binding.getRoot()).
                        navigate(SchoolsListFragmentDirections.actionSchoolsListFragmentToSchoolDetailFragment(school));
            }
        });
        binding.recyclerView.setAdapter(adapter);

        mViewModel.getSchoolsList().observe(this, new Observer<List<School>>() {
            @Override
            public void onChanged(List<School> schools) {
                adapter.submitList(schools);
            }
        });

        return binding.getRoot();
    }
}
