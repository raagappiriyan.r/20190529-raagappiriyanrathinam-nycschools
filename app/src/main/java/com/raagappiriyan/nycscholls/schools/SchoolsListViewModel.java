package com.raagappiriyan.nycscholls.schools;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.raagappiriyan.nycscholls.data.School;
import com.raagappiriyan.nycscholls.network.NYCSchoolsApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SchoolsListViewModel extends ViewModel {
    public SchoolsListViewModel() {
        getAllSchools();
    }

    private MutableLiveData<List<School>> mSchools = new MutableLiveData<>();

    LiveData<List<School>> getSchoolsList() {
        return mSchools;
    }

    private void getAllSchools() {
        NYCSchoolsApiService.sNYCSchoolsApi.getSchools().enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(@NonNull Call<List<School>> call,
                                   @NonNull Response<List<School>> response) {
                mSchools.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<List<School>> call,
                                  @NonNull Throwable t) {
            }
        });
    }
}
