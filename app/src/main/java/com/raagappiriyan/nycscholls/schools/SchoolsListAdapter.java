package com.raagappiriyan.nycscholls.schools;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.raagappiriyan.nycscholls.data.School;
import com.raagappiriyan.nycscholls.databinding.SchoolListItemBinding;

public class SchoolsListAdapter extends ListAdapter<School, SchoolsListAdapter.SchoolItemViewHolder> {
    private OnItemSelectListener mListener;

    SchoolsListAdapter(OnItemSelectListener listener) {
        super(new DiffCallback());
        mListener = listener;
    }

    @NonNull
    @Override
    public SchoolItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return SchoolItemViewHolder.from(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolItemViewHolder holder, int position) {
        final School school = getItem(position);
        holder.bind(school);
        holder.mBinding.getRoot().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemSelected(school);
            }
        });
    }

    interface OnItemSelectListener {
        void onItemSelected(School school);
    }

    static class SchoolItemViewHolder extends RecyclerView.ViewHolder {
        private SchoolListItemBinding mBinding;

        private SchoolItemViewHolder(@NonNull SchoolListItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(School school) {
            mBinding.setSchool(school);
            mBinding.executePendingBindings();
        }

        static SchoolItemViewHolder from(ViewGroup parent) {
            return new SchoolItemViewHolder(SchoolListItemBinding.
                    inflate(LayoutInflater.from(parent.getContext())));
        }
    }
}
