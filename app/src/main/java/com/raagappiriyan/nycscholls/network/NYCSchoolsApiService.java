package com.raagappiriyan.nycscholls.network;

import com.raagappiriyan.nycscholls.data.SATScore;
import com.raagappiriyan.nycscholls.data.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class NYCSchoolsApiService {
    private static final String BASE_URL = "https://data.cityofnewyork.us/";

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    public static NYCSchoolsApi sNYCSchoolsApi = retrofit.create(NYCSchoolsApi.class);

    public interface NYCSchoolsApi {
        @GET("resource/s3k6-pzi2.json")
        Call<List<School>> getSchools();

        @GET("resource/f9bf-2cp4.json")
        Call<List<SATScore>> getScore(@Query("dbn") String dbn);
    }
}
