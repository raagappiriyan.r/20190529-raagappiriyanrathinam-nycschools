package com.raagappiriyan.nycscholls.schoolDetail;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.raagappiriyan.nycscholls.data.SATScore;
import com.raagappiriyan.nycscholls.data.School;
import com.raagappiriyan.nycscholls.network.NYCSchoolsApiService;

import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class SchoolDetailViewModel extends ViewModel {
    private MutableLiveData<School> mSchool = new MutableLiveData<>();
    private MutableLiveData<List<SATScore>> mScoreList = new MutableLiveData<>();

    SchoolDetailViewModel(School school) {
        mSchool.setValue(school);
        getScoreData();
    }

    LiveData<School> getSchool() {
        return mSchool;
    }

    LiveData<SATScore> getScore() {
        return Transformations.map(mScoreList, new Function<List<SATScore>, SATScore>() {
            @Override
            public SATScore apply(List<SATScore> scoreList) {
                return scoreList == null || scoreList.isEmpty() ? null : scoreList.get(0);
            }
        });
    }

    private void getScoreData() {
        NYCSchoolsApiService.sNYCSchoolsApi.getScore(Objects.requireNonNull(mSchool.getValue()).mDbn)
                .enqueue(new Callback<List<SATScore>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<SATScore>> call,
                                           @NonNull Response<List<SATScore>> response) {
                        mScoreList.setValue(response.body());
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<SATScore>> call,
                                          @NonNull Throwable t) {
                    }
                });
    }
}
