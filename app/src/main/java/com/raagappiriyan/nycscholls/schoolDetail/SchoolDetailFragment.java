package com.raagappiriyan.nycscholls.schoolDetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.raagappiriyan.nycscholls.data.SATScore;
import com.raagappiriyan.nycscholls.data.School;
import com.raagappiriyan.nycscholls.databinding.FragmentSchoolDetailBinding;

public class SchoolDetailFragment extends Fragment {
    private SchoolDetailViewModel mViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        mViewModel = ViewModelProviders.of(this,
                new SchoolDetailViewModelFactory(SchoolDetailFragmentArgs.
                        fromBundle(getArguments()).getSchoolItem())).get(SchoolDetailViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        final FragmentSchoolDetailBinding binding = FragmentSchoolDetailBinding.
                inflate(inflater, container, false);
        binding.setLifecycleOwner(this);

        mViewModel.getSchool().observe(this, new Observer<School>() {
            @Override
            public void onChanged(School school) {
                binding.setSchool(school);
            }
        });

        mViewModel.getScore().observe(this, new Observer<SATScore>() {
            @Override
            public void onChanged(SATScore satScore) {
                binding.setScore(satScore);
            }
        });

        return binding.getRoot();
    }
}
