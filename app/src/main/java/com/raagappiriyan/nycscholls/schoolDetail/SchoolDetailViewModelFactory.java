package com.raagappiriyan.nycscholls.schoolDetail;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.raagappiriyan.nycscholls.data.School;

public class SchoolDetailViewModelFactory implements ViewModelProvider.Factory {
    private School mSchool;

    public SchoolDetailViewModelFactory(School school) {
        mSchool = school;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new SchoolDetailViewModel(mSchool);
    }
}
