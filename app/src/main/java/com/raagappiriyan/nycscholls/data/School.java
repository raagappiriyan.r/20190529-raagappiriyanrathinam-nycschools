package com.raagappiriyan.nycscholls.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class School implements Parcelable {
    @SerializedName("dbn")
    public String mDbn;

    @SerializedName("school_name")
    public String mSchoolName;

    @SerializedName("overview_paragraph")
    public String mOverview;

    @SerializedName("website")
    public String mWebsite;

    protected School(Parcel in) {
        mDbn = in.readString();
        mSchoolName = in.readString();
        mOverview = in.readString();
        mWebsite = in.readString();
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDbn);
        dest.writeString(mSchoolName);
        dest.writeString(mOverview);
        dest.writeString(mWebsite);
    }
}
