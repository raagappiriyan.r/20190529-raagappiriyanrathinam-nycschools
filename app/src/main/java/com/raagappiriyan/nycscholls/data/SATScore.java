package com.raagappiriyan.nycscholls.data;

import com.google.gson.annotations.SerializedName;

public class SATScore {
    @SerializedName("dbn")
    public String mDbn;

    @SerializedName("sat_critical_reading_avg_score")
    public String mReadingScore;

    @SerializedName("sat_math_avg_score")
    public String mMathScore;

    @SerializedName("sat_writing_avg_score")
    public String mWritingScore;
}
